@echo off
cls
echo ====================================
echo Simple diag mode switch for Android
echo by BuGi
echo.
echo Tested on LH ROM for Mi2s.
echo http://aloneinthespace.cf/s/
echo.
echo contact: bugi@aloneinthespace.cf
echo.
echo License: MIT 
echo https://opensource.org/licenses/MIT
echo ====================================

adb >nul 2>&1
IF %ERRORLEVEL% NEQ 1 (
	echo ADB not found
	pause
	exit
)

echo.
echo Activate root:
echo  go to Developer options - Root access
echo  change setting to "Adb only" or "Apps and ADB"
echo.
echo When it's ready
pause

echo.
adb devices

echo.
echo Switching to root...
adb root

echo.
echo Waiting for device to come back...
adb wait-for-device

echo.
echo Ok, switching to diag mode...
adb shell setprop sys.usb.config mtp,diag,diag_mdm,adb

echo.
echo Done, everything should be fine now.
echo When you finish simply reboot your device.
echo.
echo Have fun.
